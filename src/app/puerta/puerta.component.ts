import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';


export interface LstRoom {
  id: number;
  nombre: string;
}

export interface LstUser {
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-puerta',
  templateUrl: './puerta.component.html',
  styleUrls: ['./puerta.component.css']
})
export class PuertaComponent implements OnInit {

  datoSend = {sala: '', accion: '', autor: ''};
  datoReceived: any;

  // var with ngModel value from html
  userSelect: string;
  roomSelect: string;
  roomUser: string;

  selectValue: string;

  // json list of rooms
  rooms: LstRoom [] = [
    { id: 1, nombre: '500' },
    { id: 2, nombre: '501' },
    { id: 3, nombre: '502' },
    { id: 4, nombre: '503' },
    { id: 5, nombre: '504' },
    { id: 6, nombre: '505' },
    { id: 7, nombre: '506' },
    { id: 8, nombre: '507' },
    { id: 9, nombre: '508' },
    { id: 10, nombre: '601' },
    { id: 11, nombre: '701' },
    { id: 12, nombre: '702' },
    { id: 13, nombre: '703' },
    { id: 14, nombre: '704' },
    { id: 15, nombre: '706' },
    { id: 16, nombre: '707' }, ] ;


  // json list of user
  users: LstUser [] = [
    { id: 1, nombre: 'Elvis Gaona' },
    { id: 2, nombre: 'Paulo Coronado' },
    { id: 3, nombre: 'Marco Alzate' },
    { id: 4, nombre: 'Raul Romero' }];

  // public selUser = this.lstUser[1].id;

  public sendText(event) {

    // this.roomUser = this.roomSelect + ';' + this.userSelect;
    this.datoSend.sala = this.roomSelect;
    this.datoSend.autor = this.userSelect;
    this.datoSend.accion = '1';

    console.log(this.datoSend);

    this.rest.Abrir(this.datoSend).subscribe((result) => {
      // this.productData = result;
       this.roomUser = result.status;
       console.log(result);
       // this.router.navigate(['/product-details/'+result._id]);
     }, (err) => {
       console.log(err);
     });

     // this.roomUser = this.datoReceived;
  }
  constructor(public rest: RestService) { }

  ngOnInit() {
  }

}
